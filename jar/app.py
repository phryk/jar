import logging

from werkzeug.datastructures import CallbackDict

from flask import Flask
from flask.sessions import SecureCookieSessionInterface, SecureCookieSession
from flask.ext.sass import Sass


from .data import Perma
from .blueprints import admin, site

class JarSession(SecureCookieSession):


    def __init__(self, initial=None):
        
        super(JarSession, self).__init__(initial)

        def on_update(self):
            print "session update"
            self.modified = True

        CallbackDict.__init__(self, initial, on_update)


    def on_update(self, *a, **kw):

        print "session.on_update:", a, kw
        die()



class Jar(Flask):

    sass = None
    ipc = None

    def run(self, *a, **kw):

        session_interface = SecureCookieSessionInterface()
        session_interface.session_class = JarSession
        self.session_interface = session_interface
        self.sass = Sass(self)

        file_handler = logging.FileHandler('jar.log')
        self.logger.addHandler(file_handler)

        if self.config['ipc']:

            from .ipc import IPCServer

            self.ipc = IPCServer(self.config['ipc_path'])
            self.logger.debug('Starting IPC Server')
            self.ipc.start()

        return super(Jar, self).run(*a, **kw)



app = Jar('jar')
import config


app.register_blueprint(site)
app.register_blueprint(admin, url_prefix='/admin')

@app.before_first_request
def boot():
    perma = Perma()
    perma.load()

@app.teardown_appcontext
def shutdown():
    if app.config['ipc']:
        app.ipc.kill()
