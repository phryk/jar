from random import SystemRandom

class FlashError(Exception):
    pass



def parse_filterset(filters):

    filterset = {'show': [], 'hide': []}
    clauses = filters.split(',')
    for clause in clauses:
        if clause.startswith('!'):
            filterset['hide'].append(clause[1:])

        else:
            filterset['show'].append(clause)

    return filterset



def build_challenge(length = 64):

    source = SystemRandom()
    source.seed()

    #TODO: Escape html special chars
    return ''.join([chr(x) for x in [source.randint(35, 126) for y in range(length)]])
