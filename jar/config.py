from os import path

from .app import app


app.secret_key = 'CHANGEME'
app.config['SASS_BIN_PATH'] = '/usr/local/bin/sass'
app.config['MEDIA_PATH'] = path.abspath('media')
app.config['site_name'] = 'jar'
app.config['title_separator'] = '::'
app.config['client_cert_lifetime'] = 2592000 # client cert stays valid for 30d
app.config['ipc'] = True
app.config['ipc_path'] = '/tmp/oink'
