from cPickle import load, dump

class Perma(dict):

    _instance = None

    def __new__(cls, *args, **kwargs):

        """
        Implements singleton behavior.
        """

        if cls._instance is None:
            cls._instance = super(Perma, cls).__new__(cls, *args, **kwargs)

        return cls._instance


    def __init__(self, *a, **kw):

        super(Perma, self).__init__(*a, **kw)

        from .content import storables

        for key in storables:

            if not self.has_key(key):
                self[key] = {}


    def load(self):

        try:
            fh = open('jar.pickle', 'r')
            data = load(fh)
            fh.close()
            for (k,v) in data.iteritems():
                self[k] = v


        except IOError:
            pass # silently pass "file not found" errors


    def reload(self):

        for k in self.iterkeys():
            del self[k]

        self.load()


    def save(self):
        return dump(self, open('jar.pickle', 'wb'))
