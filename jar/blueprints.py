from time import time
from collections import OrderedDict
from M2Crypto import X509, EVP
from pyspkac import SPKAC
from functools import wraps
from werkzeug.wrappers import Response
from flask import Blueprint, abort, flash, g, get_flashed_messages, redirect, render_template, request, session, url_for

from .data import Perma
from .content import Comment, Post, Tag, Listing, Form, StringField, KeygenField, storables
from .menu import Menu
from .utils import FlashError, parse_filterset, build_challenge

perma = Perma()




def load_storable(f):

    @wraps(f)
    def wrapper(*a, **kw):

        if not kw.has_key('storable'):
            raise FlashException('Routes using @load_storable need to name the class parameter "storable".')

        key = kw['storable']
        if not storables.has_key(key):
            abort(404, 'No such storable.')

        else:

            kw['storable'] = storables[key]

            return f(*a, **kw)

    return wrapper




# Menu generation callbacks

def menu_site(*a, **kw):

    menu = Menu()
    menu.add_item('Home', url_for('site.site_front'))
    menu.add_item('Posts', url_for('site.site_storable', storable=Post._key))
    menu.add_item('Tags', url_for('site.site_storable', storable=Tag._key))

    return menu



def menu_admin(*a, **kw):

    menu = Menu()
    menu.add_item('Site Home', url_for('site.site_front'))
    menu.add_item('Admin Home', url_for('admin.admin_front'))

    return menu



def menu_storable_secondary(*a, **kw):

    storable = storables[kw['storable']]

    instance = storable()

    menu = Menu()
    menu.add_item('List', url_for('admin.admin_storable_list_all', storable=storable._key))
    menu.add_item('Add', instance.url('add'))

    return menu



def menu_storable_tertiary(*a, **kw):

    storable = storables[kw['storable']]

    menu = Menu()

    if perma[storable._key].has_key(kw['id']):
        instance = perma[storable._key][kw['id']]

        menu.add_item('View', url_for('site.site_storable_item', storable=storable._key, id=kw['id']))
        menu.add_item('Edit', url_for('admin.admin_storable_edit', storable=storable._key, id=kw['id']))
        menu.add_item('Delete', url_for('admin.admin_storable_delete', storable=storable._key, id=kw['id']))
   
    return menu




class JarPrint(Blueprint):

    app = None
    menu_callbacks = None

    
    def __init__(self, *a, **kw):

        if kw.has_key('menu_callbacks'):
            self.menu_callbacks = kw.pop('menu_callbacks')
        else:
            self.menu_callbacks = {}
        
        #from .app import app # This might be ugly. Move function into app object?
        #self.app = app

        super(JarPrint, self).__init__(*a, **kw)


    def view(self, secondary=None, tertiary=None):

        """
        Decorator to enable page functions to return a renderable object which
        is to be rendered within the boilerplate template(s).
        """

        def decorator(f):

            @wraps(f)
            def wrapper(*a, **kw):


                g.menus = {}

                for name, callback in self.menu_callbacks.iteritems():
                    g.menus[name] = callback(*a, **kw)

                if secondary:
                    g.menus['secondary'] = secondary(*a, **kw)

                if tertiary:
                    g.menus['tertiary'] = tertiary(*a, **kw)

                value = f(*a, **kw)
                if type(value) is Response:
                    return value

                title = [self.app.config['site_name']]
                title.append(value.title)

                title = self.app.config['title_separator'].join(title)

                return render_template('main.jinja', title=title, content=value, menus=g.menus, messages=get_flashed_messages(True))

            return wrapper

        return decorator


#    def menu(self, name):
#
#        #TODO: Complete this decorator
#        def decorator(f):
#
#            self.add_menu(name, f)

#       use this to get path of main application file
#        import __main__
#        from os import path
#        print "__main__:", dir(__main__)
#        print path.dirname(path.abspath(__main__.__file__))

    def add_menu(self, name, callback):

        self.menu_callbacks[name] = callback


#    def block(self, 
#
#
#    def add_block(self, name, callback):
#       able to manually check route against url?


    def make_setup_state(self, app, options, first_registration=False):

        self.app = app
        return super(JarPrint, self).make_setup_state(app, options, first_registration=first_registration)


# main site

site = JarPrint('site', 'jar')
site.add_menu('main', menu_site)

@site.route('/favicon.ico')
def site_favicon():

    return "TODO: dynamic favicon 'n shit"



@site.route('/')
@site.view()
def site_front():
    return site_filter()



@site.route('/filter/')
def site_filter_empty():
    return site_filter()



@site.route('/filter/<string:filters>/')
def site_filter(filters = ''):

    filterset = parse_filterset(filters)
    return Listing(Post, filterset=filterset)



@site.route('/<string:storable>/')
@site.view()
@load_storable
def site_storable(storable):

    return Listing(storable)



@site.route('/<string:storable>/<string:id>/')
@site.view()
@load_storable
def site_storable_item(storable, id):
    
    if not id in perma[storable._key].keys():
        abort(404, 'This is not the %s you were looking for.' % (storable.__name__,))
    
    return perma[storable._key][id]


@site.route('/posts/<string:id>/comment', methods=['POST'])
def site_comment(id):

    if not id in perma[Post._key].keys():
        abort(404, 'This is not the %s you were looking for.' % (Post.__name__,))

    post = perma[Post._key][id]
    comment = Comment(post)
    comment.nick = request.form['nick']
    comment.social = request.form['social']
    comment.text = request.form['text']

    comment.save()

    return redirect(url_for('site.site_storable_item', storable=Post._key, id=id))
    



# admin section

admin = JarPrint('admin', 'jar')
admin.add_menu('main', menu_admin)


@admin.route('/register')
@admin.view()
def admin_register():

    challenge = build_challenge()

    form = Form('register')
    form.fields['username'] = StringField('username', 'Username')
    form.fields['key'] = KeygenField('key', 'Keygen', challenge=challenge)

    session['register_challenge'] = challenge
    session['register_challenge_timestamp'] = time()
    admin.app.logger.debug(session['register_challenge'])
    admin.app.logger.debug(session['register_challenge_timestamp'])

    admin.app.logger.debug('updated session')


    return form


@admin.route('/register', methods=['POST'])
def admin_register_post():

    from _ssl import ca_key, ca_crt

    admin.app.logger.debug(ca_key)

    pkey = EVP.load_key_string(ca_key)
    cert = X509.load_cert_string(ca_crt)

    username = request.form['username']
    spkac_encoded = request.form['key']

    spkac = SPKAC(spkac_encoded, session['register_challenge'], CN=username, Email='fnord@fnord.fnord')

    spkac.push_extension(X509.new_extension('keyUsage', 'digitalSignature, keyEncipherment, keyAgreement', critical=True))
    spkac.push_extension(X509.new_extension('extendedKeyUsage', 'clientAuth, emailProtection, nsSGC'))

    spkac.subject.C = cert.get_subject().C

    not_before = int(time())
    not_after = not_before + admin.app.config['client_cert_lifetime']

    client_cert = spkac.gen_crt(pkey, cert, 44, not_before, not_after, hash_algo='sha512')

    print "CLIENT CERT: ", client_cert.as_pem()


    return Response(client_cert.as_der(), mimetype='application/x-x509-user-cert')#redirect(url_for('.admin_register'))


@admin.route('/')
@admin.view()
def admin_front():
    
    menu = Menu(title='Administration')

    for key, storable in storables.iteritems():
        menu.add_item(storable.__name__, url_for('.admin_storable_list_all', storable=key))

    return menu



@admin.route('/%s/filter/<string:filters>/' % (Post._key,))
@admin.view()
def admin_content_list_filtered(filters = ''):

    filterset = parse_filterset(filters)
    return Listing(Post, listmode='edit-teaser', filterset=filterset)



## Storable Administration
@admin.route('/<string:storable>/')
@admin.view(secondary=menu_storable_secondary)
@load_storable
def admin_storable_list_all(storable):

    return Listing(storable, listmode='edit-teaser')



@admin.route('/<string:storable>/add')
@admin.view(secondary=menu_storable_secondary)
@load_storable
def admin_storable_add(storable):

    return  storable().form()



@admin.route('/<string:storable>/add', methods=['POST'])
@load_storable
def admin_storable_add_post(storable):

    item = storable()
    form = item.form()

    try:
        form.handle()
        item.save()

    except FlashError as e:
        flash(e.message, 'error')
        return redirect('.') # No clue if this actually works. Test it, maybe?
    return redirect('.') # No clue if this actually works. Test it, maybe?

    #return redirect(url_for('.admin_storable_list_all', storable=storable._key))



@admin.route('/<string:storable>/<string:id>/')
@admin.view(secondary=menu_storable_secondary, tertiary=menu_storable_tertiary)
@load_storable
def admin_storable_edit(storable, id):

    if not id in perma[storable._key].keys():
        abort(404, 'No such %s.' % (storable._key,))

    item = perma[storable._key][id]

    return item.form()



@admin.route('/<string:storable>/<string:id>/', methods=['POST'])
@load_storable
def admin_storable_edit_post(storable, id):

    item = perma[storable._key][id]


    try:
        form.handle()
        item.save()

    except Exception as e:
        msg = "An error occured while trying to handle this form.%s: %s" % (e.__class__.__name__, e.message)
        flash(msg, 'error')

    return redirect(item.url('edit'))



@admin.route('/<string:storable>/<string:id>/delete')
@admin.view(secondary=menu_storable_secondary, tertiary=menu_storable_tertiary)
@load_storable
def admin_storable_delete(storable, id):

    if not id in perma[storable._key].keys():
        abort(404, 'No such %s.' % (storable._key,))

    post = perma[storable._key][id]

    return post.form('delete')



@admin.route('/<string:storable>/<string:id>/delete', methods=['POST'])
@load_storable
def admin_storable_delete_post(storable, id):

    if not id in perma[storable._key].keys():
        abort(404, 'No such post.')

    post = perma[storable._key][id]
    title = post.title

    try:
        post.delete()

    except FlashError as e:
        flash(e.message, 'error')

    flash("Post '%s' successfully deleted." % (title,))

    return redirect(url_for('.admin_storable_list_all', storable=storable._key))
