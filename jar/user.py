from .data import Perma
from .content import Renderable

class User(Renderable):

    certs = None # list, certificate serial numbers that authenticate this user
    coms = None # dictionary, contact options
    about = None # str

    def __init__(self):

