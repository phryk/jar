from os import path
from collections import OrderedDict
from sets import Set
from flask import render_template, request, url_for
from werkzeug import secure_filename

from .utils import FlashError
from .data import Perma



class Renderable(object):

    """
    Renderable base class.
    """

    _rendering = None
    title = None

    def __init__(self, title=None):

        self._rendering = False

        if title:
            self.title = title
        else:
            self.title = self.__class__.__name__


    def render(self, mode='full', template=None):

        if self._rendering is True:
            # Is this actually needed?
            self._rendering = False
            raise Exception('Recursive rendering forbidden.')
            

        if not template:
            template = '%s-%s.jinja'% (self.__class__.__name__.lower(), mode)

        self._rendering = True
        rendered_self = render_template(template, data=self)
        self._rendering = False

        return rendered_self



# form stuff below this

class Form(Renderable):

    name = None
    action = None # url used as form action
    mode = None
    fields = None
    parent = None # The storable object this form is used for

    def __init__(self, name, mode='add', parent=None):

        super(Form, self).__init__()

        if parent:
            self.parent = parent
            self.action = parent.url(mode)
        else:
            self.action = ''

        self.name = name
        self.mode = mode
        self.fields = OrderedDict()


    def validate(self):

        for name, field in self.fields.iteritems():
            if not field.validate():
                return False

        return True


    def handle(self):

        for name, field in self.fields.iteritems():

            if field.__class__ is FileField:
                value = request.files[name]
            else:
                #if name == 'id' and not request.form.has_key('id') and self.parent.id:
                if field.blocked and not request.form.has_key(name) and getattr(self.parent, name):
                    field.value = getattr(self.parent, name)

                else:
                    value = request.form[name]
                    field.value = value


        if not self.validate():
            raise FlashError("Form '%s' did not validate." % (self.name,))

        for name, field in self.fields.iteritems():
            field.handle(self.parent)


    def render(self, mode='full', template=None):

        if not template:
            template = ['form-%s.jinja' % (self.name,), 'form.jinja']


        return super(Form, self).render(mode, template)



class FormField(Renderable):

    """
    Base form field.
    """

    _types = None
    type = None
    name = None
    label = None
    value = None
    obligatory = None
    blocked = None

    def __init__(self, name, label=None, value=None, obligatory=False, blocked=False):

        self.type = self.__class__.__name__.lower()
        self.name = name
        
        if label:
            self.label = label
        else:
            self.label = name.title()

        self.value = value
        self.obligatory = obligatory
        self.blocked = blocked


    def is_empty(self):
        return not self.value


    def validate(self):

        if self.is_empty():
            return not self.obligatory

        return type(self.value) in self._types


    def nice_value(self):

        return self.value


    def handle(self, target):

        print "handling file field"
        print self.name
        print target
        print self.value
        
        setattr(target, self.name, self.value)


    def render(self, mode='full', template=None):

        if not template:
            template = ['form-element-%s.jinja' % (self.__class__.__name__.lower(),), 'form-element.jinja']

        return super(FormField, self).render(mode, template)



class StringField(FormField):

    def __init__(self, *a, **kw):

        self._types = [str, unicode]
        super(StringField, self).__init__(*a, **kw)



class TextField(StringField):

    pass



class FileField(FormField):

    basepath = None

    def __init__(self, *a, **kw):

        from .app import app
        super(FileField, self).__init__(*a, **kw)
        self.basepath = app.config['MEDIA_PATH']


    def is_empty(self):
        return not self.value.filename


    def validate(self):

        if self.is_empty():
            return not self.obligatory

        return True


    def handle(self, target):

        print "Filefield handler!"
        print dir(self.value)

        print self.value.name
        print self.value.filename
        print self.value.mimetype_params
        print self.value.content_type

        if not self.is_empty():

            filename = secure_filename(self.value.filename)
            self.value.save(path.join(self.basepath, filename))

            target.file = self.value
            target.content_type = self.value.content_type
            target.mimetype_params = self.value.mimetype_params
            print "SAVED DAT SHIT"



class KeygenField(FormField):

    challenge = None

    def __init__(self, *a, **kw):

        if not kw.has_key('challenge'):
            raise Exception('%s must be initialized with challenge kw param.' % (self.__class__.__name__,))

        self.challenge = kw.pop('challenge')
        super(KeygenField, self).__init__(*a, **kw)



class TagField(StringField):

    _perma = None

    def __init__(self, *a, **kw):

        super(TagField, self).__init__(*a, **kw)

        self._perma = Perma()

    def nice_value(self):

            return ', '.join(self.value.keys())

    def handle(self, target):

        tags = OrderedDict()
        taglist = self.value.split(',')
        for tag_id in taglist:
            tag_id = tag_id.strip()

            if tag_id in self._perma[Tag._key].keys():
                tag = self._perma[Tag._key][tag_id]

            else:
                tag = Tag(tag_id, tag_id.title())
                tag.save()

            tags[tag.id] = tag

        setattr(target, self.name, tags)




# Actual content and other non-form renderables

class Storable(Renderable):

    """
    Storable base class.
    """

    _perma = None
    _fields = None
    _key = None
    id = None


    def __init__(self, id=None, title=None):

        super(Storable, self).__init__()

        self._perma = Perma()

        if id:
            self.id = id

        if title:
            self.title = title
        else:
            self.title = ''


    def url(self, mode='view'):

        if mode == 'view':
            return url_for('site.site_storable_item', storable=self._key, id=self.id)

        elif mode == 'add':
            return url_for('admin.admin_storable_add', storable=self._key)

        elif mode == 'edit':
            return url_for('admin.admin_storable_edit', storable=self._key, id=self.id)


    def form(self, mode=None):

        """
        Return a fully filled Form object, representing this Storable instance.
        """

        if not mode:
            if self.id:
                mode = 'edit'
            else:
                print "ID: ", self.id
                mode = 'add'

        form = Form(self.__class__.__name__.lower(), mode=mode, parent=self)

        for (name, Element) in self._fields.iteritems():

            value = getattr(self, name)
            blocked = False
            obligatory = False

            if name == 'id':
                obligatory = True

                if mode == 'edit':
                    blocked = True

            form.fields[name] = Element(name, value=value, obligatory=obligatory, blocked=blocked)


        return form


    def save(self):

        if not self.id:
            raise FlashError("Can't save this %s, no id given." % (self.__class__.__name__,))

        if not self._perma[self._key].has_key(self.id) or \
        self._perma[self._key][self.id] is self:
            self._perma[self._key][self.id] = self #strictly speaking, this line is only needed when adding a new item, not on update
            self._perma.save()
        
        else:
            raise FlashError('A %s with id %s already exists.' % (self.__class__.__name__, self.id))


    def delete(self):

        if self._perma[self._key].has_key(self.id):
            self._perma[self._key].pop(self.id)
            self._perma.save()



class File(Storable):

    _key = 'files'
    _fields = OrderedDict([('id', StringField), ('file', FileField)])    

    content_type = None
    mimetype_params = None
    file = None


    def form(self, mode='add'):

        form = super(File, self).form(mode)

        if mode == 'add':
            form.fields['file'].obligatory = True

        return form



class Tag(Storable):

    _key = 'tags'
    _fields = OrderedDict([('id', StringField), ('title', StringField), ('description', TextField)])

    id = None
    description = None

    def __init__(self, id=None, title=None, description=None):

        super(Tag, self).__init__(id, title)

        if description:
            self.description = description
        else:
            self.description = ''


    def get_posts(self):

        filterset = {'hide':[], 'show':[self.id]}
        listing = Listing(Post, filterset=filterset)

        print "GET POSTS: ", listing.items

        return listing



class Comment(Storable):

    _key = 'comments'
    _fields = OrderedDict([('nick', StringField), ('social', StringField), ('text', TextField)])

    post = None
    nick = None
    social = None
    text = None

    def __init__(self, post, nick='', social='', text=''):

        super(Comment, self).__init__()

        self.post = post
        self.nick = nick
        self.social = social
        self.text = text


    def save(self):

#        if not self.id:
#            raise FlashError("Can't save this %s, no id given." % (self.__class__.__name__,))

        if not self._perma[self._key].has_key(self.post.id):
            self._perma[self._key][self.post.id] = []

        if not self in self._perma[self._key][self.post.id]:
            self._perma[self._key][self.post.id].append(self)

        self._perma.save()



class Post(Storable):

    _key = 'posts'
    _fields = OrderedDict([('id', StringField), ('title', StringField), ('tags', TagField), ('text', TextField)])

    id = None
    tags = None
    text = None


    def __init__(self, id=None, title=None, text=None, tags=None):

        super(Post, self).__init__(id, title)

        if text:
            self.text = text
        else:
            self.text = ''

        if tags:
            self.tags = tags
        else:
            self.tags = OrderedDict()


    def get_comments(self):

        comments = []
        if self._perma[Comment._key].has_key(self.id):
            comments = self._perma[Comment._key][self.id]

        return comments


#    def form(self, mode='add'):
#
#        form = super(Post, self).form(mode)
#
#        if mode == 'edit' and len(self.tags):
#            print "FORM THINGIE: ", form.fields['tags']
#            form.fields['tags'].value = ', '.join(self.tags.keys())
#
#        return Container([form, self.file_form()])


    def comment_form(self):

        return Comment(self).form()


    def file_form(self):
        return File().form()



class Listing(Renderable):

    _perma = None
    items = None
    listmode = None


    def __init__(self, cls, listmode='teaser', items=None, **kw):

        super(Listing, self).__init__()

        self._perma = Perma()
        self.listmode = listmode

        if items:
            self.items = items

        else:
            self.items = []

            for item in self._perma[cls._key].itervalues():
                if kw.has_key('filterset'):
                    filterset = kw['filterset']

                    set_tags = Set(item.tags.keys())
                    set_filters = Set(filterset['hide'])

                    intersection_hide = set_filters.intersection(set_tags)
                    if len(intersection_hide):
                        pass

                    set_filters = Set(filterset['show'])
                    intersection_show = set_filters.intersection(set_tags)
                    if len(intersection_show):
                        self.add_item(item)

                else:
                    self.add_item(item)


    def add_item(self, item):
        self.items.append(item)



class Container(Renderable, list):

    def __init__(self, l=[]):

        super(Container, self).__init__()

        for item in l:
            self.append(item)


    def render(self, mode='full', template=None):

        r = u''

        for item in self:
            r += item.render()

        return r




# This module variable is allowed to be imported and modified.
# The intention is enabling an application using jar to add
# its own storable classes. This is where jar looks for
# the storable classes that can be CRUD'ed on the site.
storables = {}

for storable in [Post, Tag, Comment, File]:
    storables[storable._key] = storable
