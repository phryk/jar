from flask import url_for

from .content import Renderable


class Menu(Renderable):

    title = None
    items = None

    def __init__(self, items=None, title=''):

        self.title = title

        if items:
            self.items = items
        else:
            self.items = []


    def add_item(self, label, path):

        self.items.append((label, path))
