from socket import socket, AF_UNIX, SOCK_STREAM, SOL_SOCKET, SO_REUSEADDR
import socket as asdf
from threading import Thread
from os import unlink, path
from .data import Perma

class IPCServer(Thread):

    daemon = True
    sock = None
    addr = None
    max_command_size = None
    die = None
    perma = None

    def __init__(self, addr, max_command_size = 4096):

        try:
            unlink(addr)
        except OSError:
            if path.exists(addr):
                raise

        sock = socket(AF_UNIX, SOCK_STREAM)
        sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        sock.bind(addr)
        sock.listen(0)

        self.sock = sock
        self.max_command_size = max_command_size
        self.die = False
        self.addr = addr
        self.perma = Perma()

        print "sock: ", self.sock.fileno()

        super(IPCServer, self).__init__()


    def run(self):
       
        while not self.die:
            self.handle_client()


    def kill(self):
        self.die = True


    def handle_client(self):

        (conn, addr) = self.sock.accept()

        while True:
            raw_command = conn.recv(self.max_command_size).decode('utf-8')
            print "ZOMG X: ", raw_command
